from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from router import users, pokemon, owned_pokemon, teams, duels, types
from authenticator import authenticator

app = FastAPI()
app.include_router(users.router)
app.include_router(pokemon.router)
app.include_router(owned_pokemon.router)
app.include_router(teams.router)
app.include_router(duels.router)
app.include_router(types.router)
app.include_router(authenticator.router)


CORS_HOST = os.environ["CORS_HOST"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_HOST,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }


@app.get("/")
def root():
    return {"message": "You hit the root path!"}
