from pydantic import BaseModel
from query.pool import pool
from typing import List, Union
from query.teams import TeamOut


class Error(BaseModel):
    message: str


class DuplicateUserError(ValueError):
    pass


class UserIn(BaseModel):
    picture: str
    username: str
    firstname: str
    lastname: str
    email: str
    password: str


class UserOut(BaseModel):
    id: int
    picture: str
    username: str
    email: str


class UserOutWithPassword(UserOut):
    hashedpassword: str


class SingleUserOut(BaseModel):
    id: int
    picture: str
    username: str
    firstname: str
    lastname: str
    email: str
    hashedpassword: str


class ListOwnedPokemonOut(BaseModel):
    id: int
    owner_id: int
    pokemon_id: int
    picture: str
    pokemon: str
    type: str
    strength: str
    weaknesses: str


class TeamsOut(BaseModel):
    id: int
    owner_id: int
    teamname: str
    pokemon1name: str
    pokemon1picture: str
    pokemon1type: str


class UserRepository:
    def get_user(self, username: str) -> UserOutWithPassword:
        try:
            print("is trying get somehow?")
            print("username", username)
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT  id,
                        picture,
                        username,
                        firstname,
                        lastname,
                        email,
                        hashedpassword
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    print("something")
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that user"}

    def get_pokemon_by_user(self, username: str) -> List[ListOwnedPokemonOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT users.id,
                        users.username,
                        user_pokemon.owner_id,
                        user_pokemon.pokemon_id,
                        pokemon.picture,
                        pokemon.id,
                        pokemon.name,
                        pokemon.type,
                        types.strengths,
                        types.weaknesses
                        FROM users
                        INNER JOIN
                        user_pokemon on
                        users.id = user_pokemon.owner_id
                        INNER JOIN
                        pokemon on
                        user_pokemon.pokemon_id = pokemon.id
                        INNER JOIN
                        types ON
                        pokemon.type = types.typename
                        WHERE users.username = %s
                        """,
                        [username],
                    )
                    return [
                        self.record_to_user_to_pokemon(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get pokemon by users"}

    def update_username(
        self, user_id: int, user: UserIn
    ) -> Union[UserOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET username = %s
                        """,
                        [user.username, user_id],
                    )

                    return self.user_in_to_out(user_id, user)
        except Exception as e:
            print(e)
            return {"message": "Could not update that user"}

    def create_user(
        self, user: UserIn, hashedpassword: str
    ) -> UserOutWithPassword:
        try:
            print("USER", user)
            print("HASHED", hashedpassword)
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (picture,
                            username,
                            email,
                            firstname,
                            lastname,
                            hashedpassword)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                         RETURNING id,
                         picture,
                         username,
                         email,
                         firstname,
                         lastname,
                         hashedpassword;
                        """,
                        [
                            user.picture,
                            user.username,
                            user.email,
                            user.firstname,
                            user.lastname,
                            hashedpassword,
                        ],
                    )
                    print("insert worked?")
                    id = result.fetchone()[0]
                    db.execute(
                        "SELECT id FROM pokemon ORDER BY RANDOM() LIMIT 1"
                    )
                    pokemonid = db.fetchone()[0]

                    db.execute(
                        """
                        INSERT INTO user_pokemon
                            (owner_id, pokemon_id)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [id, pokemonid],
                    )
                    print("ID GOTTEN", id)
                    return UserOutWithPassword(
                        id=id,
                        picture=user.picture,
                        username=user.username,
                        email=user.email,
                        hashedpassword=hashedpassword,
                    )
        except Exception:
            return {"message": "Create did not work"}

    def get_teams(self, id: int) -> List[TeamsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT users.id,
                        users.username,
                        teams.owner_id,
                        teams.id,
                        teams.teamname,
                        teams.pokemon1,
                        pokemon.name,
                        pokemon.picture,
                        pokemon.type
                        FROM users
                        JOIN teams ON users.id = teams.owner_id
                        LEFT JOIN pokemon ON teams.pokemon1 = pokemon.id
                        WHERE users.id = %s
                        """,
                        [id],
                    )
                    return [
                        self.record_to_test_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get teams"}

    def record_to_test_out(self, record):
        return TeamsOut(
            id=record[3],
            owner_id=record[2],
            teamname=record[4],
            pokemon1name=record[6],
            pokemon1picture=record[7],
            pokemon1type=record[8],
        )

    def user_in_to_out(self, id: int, user: UserIn):
        old_data = user.dict()
        return UserOut(id=id, **old_data)

    def record_to_user_out(self, record) -> UserOutWithPassword:
        print(record)
        user_dict = {
            "id": record[0],
            "picture": record[1],
            "username": record[2],
            "email": record[3],
            "hashedpassword": record[6],
        }
        return user_dict

    def record_to_team_out(self, record):
        return TeamOut(id=record[0], owner_id=record[1], teamname=record[2])

    def record_to_user_to_pokemon(self, record):
        return ListOwnedPokemonOut(
            id=record[0],
            owner_id=record[2],
            picture=record[4],
            pokemon_id=record[5],
            pokemon=record[6],
            type=record[7],
            strength=record[8],
            weaknesses=record[9],
        )

    def record_to_single_user_out(self, record) -> UserOutWithPassword:
        print(record)
        user_dict = {
            "id": record[0],
            "picture": record[1],
            "username": record[2],
            "firstname": record[3],
            "lastname": record[4],
            "email": record[5],
            "hashedpassword": record[6],
        }
        return user_dict
