import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

function CreateDuel() {
  const [teams, setTeams] = useState([]);
  const [user, setUser] = useState('');
  const [submit, setSubmitted] = useState(false);
  const navigate = useNavigate();

  async function handleSubmit(event, team) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }
    const userid = user.id
    const UserData = {
      userid: userid,
      teamid: team.id
    };
    const duelsurl = `${process.env.REACT_APP_API_HOST}/duels`
    const patchConfig = {
      method: "post",
      body: JSON.stringify(UserData),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    fetch(duelsurl, patchConfig)
      .then((response) => response.json())
      .then((data) => {
        event.target.reset()
        setSubmitted(true)
        setTimeout(function () {
          console.log(data)
          navigate(`/duels/${data.id}`)
        }, 5000)
      })
  };

  const TeamCard = ({ team, onSubmitHandler }) => {
    return (
      <>
        <div className="team-card" onClick={() => onSubmitHandler({ target: { reset: () => { } } }, team)}>
          <h5 className="teamname">{team.teamname}</h5>
          <h6><img src={team.pokemon1picture} className="list-picture" alt={team.pokemon1picture} /> {team.pokemon1name}</h6>
        </div>
      </>
    );
  };



  useEffect(() => {
    const fetchTeams = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/teams/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setTeams(data);
        })
        .catch((error) => console.error(error));
    };
    const fetchUser = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setUser(data);
        })
        .catch((error) => console.error(error));
    };

    fetchUser()
    fetchTeams()
  }, []);


  return (
    <div className="row-form">
      <div className="offset-4 col-4">
        <div className="bg-white shadow p-4 mt-4">
          <div className="text-center">
            <h1>Random Challenger ❓</h1>
          </div>
          <div className="team-selector">
            <h5>Select team ↘️</h5>
          </div>
          <div className="container">
            {teams.map((team, idx) => (
              <TeamCard key={team.id} team={team} idx={idx} onSubmitHandler={handleSubmit} />
            ))}
          </div>
          {submit &&
            <div className="finding-duel">
              <div className="loading-message">
                <h2>Finding duel....</h2>
                <div className="bar">
                  <div className="in"></div>
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  );
};

export default CreateDuel;
