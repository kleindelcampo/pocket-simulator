steps = [
    [
        """
        CREATE TABLE types (
            id SERIAL PRIMARY KEY NOT NULL,
            image VARCHAR(1000),
            typename VARCHAR(1000),
            strengths VARCHAR(1000),
            weaknesses VARCHAR(1000)

        );

        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Pok%C3%A9mon_Electric_Type_Icon.svg/640px-Pok%C3%A9mon_Electric_Type_Icon.svg.png', 'Electric', 'Water', 'Rock');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://image.pngaaa.com/886/6175886-middle.png', 'Fire', 'Grass', 'Water');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Pok%C3%A9mon_Water_Type_Icon.svg/2048px-Pok%C3%A9mon_Water_Type_Icon.svg.png', 'Water', 'Fire', 'Electric');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://www.giantbomb.com/a/uploads/square_small/59/594908/3336342-3204262205-Grass.png', 'Grass', 'Ground', 'Fire');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Pok%C3%A9mon_Dark_Type_Icon.svg/2048px-Pok%C3%A9mon_Dark_Type_Icon.svg.png', 'Dark', 'Psychic', 'Fighting');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Pok%C3%A9mon_Fighting_Type_Icon.svg/1200px-Pok%C3%A9mon_Fighting_Type_Icon.svg.png', 'Fighting', 'Normal', 'Flying');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Pok%C3%A9mon_Normal_Type_Icon.svg/2048px-Pok%C3%A9mon_Normal_Type_Icon.svg.png', 'Normal', 'Ghost', 'Fighting');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Pok%C3%A9mon_Ground_Type_Icon.svg/768px-Pok%C3%A9mon_Ground_Type_Icon.svg.png', 'Ground', 'Rock', 'Water');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Pok%C3%A9mon_Rock_Type_Icon.svg/2048px-Pok%C3%A9mon_Rock_Type_Icon.svg.png', 'Rock', 'Ice', 'Grass');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Pok%C3%A9mon_Ice_Type_Icon.svg/480px-Pok%C3%A9mon_Ice_Type_Icon.svg.png', 'Ice', 'Dragon', 'Fire');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e8ddc4da-23dd-4502-b65b-378c9cfe5efa/dffvl73-294f6e5b-aad2-484f-bde8-1ecf082f1dfe.png/v1/fill/w_1280,h_1280/bug_type_symbol_galar_by_jormxdos_dffvl73-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTI4MCIsInBhdGgiOiJcL2ZcL2U4ZGRjNGRhLTIzZGQtNDUwMi1iNjViLTM3OGM5Y2ZlNWVmYVwvZGZmdmw3My0yOTRmNmU1Yi1hYWQyLTQ4NGYtYmRlOC0xZWNmMDgyZjFkZmUucG5nIiwid2lkdGgiOiI8PTEyODAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.msN6ZkYf5XuPiA27qO-1Zaow3B4iSRqp3nAHzctfBW0', 'Bug', 'Psychic', 'Flying');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Pok%C3%A9mon_Psychic_Type_Icon.svg/1200px-Pok%C3%A9mon_Psychic_Type_Icon.svg.png', 'Psychic', 'Fighting', 'Dark');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Pok%C3%A9mon_Steel_Type_Icon.svg/2048px-Pok%C3%A9mon_Steel_Type_Icon.svg.png', 'Steel', 'Ice', 'Fire');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://image.pngaaa.com/884/6175884-middle.png', 'Ghost', 'Psychic', 'Dark');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/e8ddc4da-23dd-4502-b65b-378c9cfe5efa/dffvl4n-1942f6ac-3f08-4dbb-a761-a722f791bc37.png/v1/fill/w_1280,h_1280/dragon_type_symbol_galar_by_jormxdos_dffvl4n-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTI4MCIsInBhdGgiOiJcL2ZcL2U4ZGRjNGRhLTIzZGQtNDUwMi1iNjViLTM3OGM5Y2ZlNWVmYVwvZGZmdmw0bi0xOTQyZjZhYy0zZjA4LTRkYmItYTc2MS1hNzIyZjc5MWJjMzcucG5nIiwid2lkdGgiOiI8PTEyODAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.Q9B0RKlPeJSmVIrfZq75vfmVscHZ50jPWPViMQp68kc', 'Dragon', 'Flying', 'Ice');
        INSERT INTO types (image, typename, strengths, weaknesses) VALUES ('https://image.pngaaa.com/494/4915494-middle.png', 'Flying', 'Bug', 'Electric');

        """,
        """
        DROP TABLE types;
        """
    ],
]
