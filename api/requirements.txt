fastapi==0.92.0
uvicorn[standard]>=0.17.6,<0.18.0
python-multipart==0.0.5
psycopg[binary,pool]
jwtdown-fastapi>=0.2.0
pytest==7.1.2
