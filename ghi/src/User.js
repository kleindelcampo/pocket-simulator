import useToken from "@galvanize-inc/jwtdown-for-react";
import React, { useEffect, useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';

function UserInfo() {
  const [user, setUser] = useState({});
  const [duels, setDuels] = useState([]);
  const navigate = useNavigate();
  const { token } = useToken();

  if (!token) {
    navigate("/")
  }

  const formatDate = (dateString) => {
    const originalDate = new Date(dateString);
    const formattedDate = `${originalDate.getMonth() + 1}/${originalDate.getDate()}/${originalDate.getFullYear()}`;
    return formattedDate;
  };

  const goToDuel = (id) => {
    navigate(`/duels/${id}`);
  }

  const UserProfile = ({ user }) => (
    <div className="offset-2 col-8">
      <div className="shadow bg-white p-4 mt-4 d-flex">
        <img src={user.picture} alt={user.name} />
        <div className="user-info d-flex">
          <div className="d-block">
            <h5>Username</h5>
            <p>{user.username}</p>
          </div>
          <div className="d-block">
            <h5>Email</h5>
            <p>{user.email}</p>
          </div>
        </div>
      </div>
    </div>
  );

  const DuelHistory = ({ duels }) => (
    <div className="offset-2 col-8">
      <div className="shadow bg-white p-4 mt-4">
        <h3>Duel History</h3>
        {duels.length === 0 ? (
          <div><p>
            No Duels
          </p>
            <Link to="/duels" className="btn btn-secondary">
              Duel!
            </Link>
          </div>
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th>Duel ID</th>
                <th>Team</th>
                <th>Enemy</th>
                <th>W / L</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {duels.map((duel) => (
                <tr className="duels-row" key={duel.duelid} onClick={() => goToDuel(duel.duelid)} >
                  <td>
                    <h5>{duel.duelid}</h5>
                  </td>
                  <td>{duel.teamname}</td>
                  <td><img src={duel.enemyimg} className="list-picture" alt={duel.enemyimg} />{duel.enemyname}</td>
                  <td><h5 style={{ color: duel.winner === 'User' ? 'green' : 'red' }}>{duel.winner === 'User' ? 'W' : 'L'}</h5></td>
                  <td>{formatDate(duel.date)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    </div>
  );

  useEffect(() => {
    const getUserData = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setUser(data);
        })
        .catch((error) => console.error(error));
    };

    const getDuels = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/duels/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setDuels(data);
        })
        .catch((error) => console.error(error));
    };

    getDuels();
    getUserData();
  }, []);

  return (
    <div>
      <div className="row-form">
        {token && <UserProfile user={user} />}
      </div>
      <div className="row">
        {token && <DuelHistory duels={duels} />}
      </div>
    </div>
  );
}

export default UserInfo;
