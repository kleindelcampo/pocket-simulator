import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";


function SignUp() {
  const defaultpic = 'https://qph.cf2.quoracdn.net/main-qimg-b1a7246edb7bb5a15f2772ad08910720.webp'
  const [username, setUsername] = useState('');
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [invalid, setInvalid] = useState(false);
  const { register } = useToken();
  const navigate = useNavigate();

  const handleRegistration = async (e) => {
    e.preventDefault();
    try {
      const picture = defaultpic
      const AccountData = {
        picture,
        username,
        firstname,
        lastname,
        email,
        password
      };
      const result = await register(
        AccountData,
        `${process.env.REACT_APP_API_HOST}/users`
      );
      if (result) {
        navigate("/")
        e.target.reset();
      } else {
        setInvalid(true)
        e.target.reset()
      }
    } catch (error) {
      setInvalid(true);
      e.target.reset()
    }
  };

  return (
    <div className="row-form">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Sign Up 📋</h1>
          <form onSubmit={(e) => handleRegistration(e)}>
            <div className="mb-3">
              <label className="form-label">Username</label>
              <input
                name="username"
                type="username"
                className="form-control"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">First Name</label>
              <input
                name="firstname"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setFirstname(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Last Name</label>
              <input
                name="lastname"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setLastname(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Email</label>
              <input
                name="email"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password</label>
              <input
                name="password"
                type="text"
                className="form-control"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
            </div>
            <div className="text-end">
              <input className="btn btn-dark" type="submit" value="Register" />
            </div>
          </form>
          {invalid && (
            <div className="error-msg alert alert-secondary" role="alert">
              Username or email taken 🛑
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SignUp;
