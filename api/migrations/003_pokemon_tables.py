steps = [
    [
        """
        CREATE TABLE pokemon (
            id SERIAL PRIMARY KEY NOT NULL,
            picture VARCHAR(1000),
            name VARCHAR(255) NOT NULL UNIQUE,
            type VARCHAR(100) NOT NULL,
            description VARCHAR(1000)
        );

        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png', 'Pikachu', 'Electric', 'When it is angered, it immediately discharges the energy stored in the pouches in its cheeks.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png', 'Charmander', 'Fire', 'The flame on its tail shows the strength of its life-force. If Charmander is weak, the flame also burns weakly.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/393.png', 'Piplup', 'Water', 'A poor walker, it often falls down. However, its strong pride makes it puff up its chest without a care.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/810.png', 'Grookey', 'Grass', 'The stick Grookey holds has grown harder and more flexible after soaking in the energy that emanates from inside its body.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/133.png', 'Eevee', 'Normal', 'Its ability to evolve into many forms allows it to adapt smoothly and perfectly to any environment.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/237.png', 'Hitmontop', 'Fighting', 'This Pokémon is adept at dance-like kicks. The horn atop its head is made from the same substance that generally forms fur and claws.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/360.png', 'Wynaut', 'Psychic', 'It tends to move in a pack. Individuals squash against one another to toughen their spirits.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/231.png', 'Phanpy', 'Ground', 'This Pokémon lives and nests on a riverbank. After playing in the mud, it is unable to settle down unless it washes its body.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/744.png', 'Rockruff', 'Rock', 'This Pokémon is very friendly at an early age. Its disposition becomes vicious once it matures, but it never forgets the kindness of its master');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/163.png', 'Hoothoot', 'Flying', 'This has an internal clock that is precise at all times. It tilts its head in a fixed rhythm.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/032.png', 'Nidoran', 'Poison', 'This is a very cautious Pokémon, always straining its large ears.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/582.png', 'Vanillite', 'Ice', 'Unable to survive in hot areas, it makes itself comfortable by breathing out air cold enough to cause snow. It burrows into the snow to sleep.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/010.png', 'Caterpie', 'Bug', 'For protection, it releases a horrible stench from the antenna on its head to drive away enemies.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/607.png', 'Litwick', 'Ghost', 'Its flame is usually out, but it starts shining when Litwick absorbs life-force from people or Pokémon.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/147.png', 'Dratini', 'Dragon', 'It sheds many layers of skin as it grows larger. During this process, it is protected by a rapid waterfall.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/246.png', 'Larvitar', 'Dark', 'Born deep underground, this Pokémon becomes a pupa after eating enough dirt to make a mountain.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/304.png', 'Aron', 'Steel', 'It eats iron ore—and sometimes railroad tracks— to build up the steel armor that protects its body.');
        INSERT INTO pokemon (picture, name, type, description) VALUES ('https://assets.pokemon.com/assets/cms2/img/pokedex/full/778.png', 'Mimikyu', 'Ghost', 'This Pokémon lives in dark places untouched by sunlight. When it appears before humans, it hides itself under a cloth that resembles a Pikachu.');
        """,

        """
        DROP TABLE pokemon;
        """,
    ],
    [
        """
        CREATE TABLE user_pokemon (
            id SERIAL PRIMARY KEY NOT NULL,
            owner_id int NOT NULL,
            pokemon_id int NOT NULL
        );
        """,

        """
        DROP TABLE user_pokemon;
        """
    ]
]
