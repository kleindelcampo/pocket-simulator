import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import './App.css';

function OwnedPokemon() {
  const [pokemon, setPokemon] = useState([]);
  const navigate = useNavigate();

  const goToPokemon = (id) => {
    navigate(`/pokemon/${id}`);
  }


  useEffect(() => {
    const getData = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/pokemon/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setPokemon(data);
        })
        .catch((error) => console.error(error));
    };
    getData()
  }, []);



  return (
    <div className="row-form">
      <div className="offset-3 col-6">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Your Pokemon</h1>
          <table className="table">
            <thead>
              <tr>
                <th>Photo</th>
                <th>ID</th>
                <th>Pokemon</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              {pokemon.map((pokemon) => (
                <tr className="pokemon-row" key={pokemon.id} onClick={() => goToPokemon(pokemon.pokemon_id)} >
                  <td>
                    <img src={pokemon.picture} className="list-picture" alt={pokemon.name} />
                  </td>
                  <td>{pokemon.pokemon_id}</td>
                  <td>{pokemon.pokemon}</td>
                  <td>{pokemon.type}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default OwnedPokemon;
