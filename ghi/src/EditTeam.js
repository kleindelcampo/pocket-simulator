import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { useParams } from 'react-router-dom';

function EditTeam() {
  const [teamname, setTeamname] = useState('');
  const [pokemon1, setPokemon1] = useState('');
  const [pokemon, setPokemon] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();
  const { id } = useParams();

  async function handleSubmit(event) {
    event.preventDefault();
    const TeamData = {
      teamname: teamname,
      pokemon1: pokemon1,
    };
    const teamurl = `${process.env.REACT_APP_API_HOST}/teams/${id}`
    const patchConfig = {
      method: "put",
      body: JSON.stringify(TeamData),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const response = await fetch(teamurl, patchConfig
    );
    console.log(response)
    console.log(TeamData)
    if (response.ok) {
      event.target.reset();
      navigate("/user/teams/protected");
    }
  };

  useEffect(() => {
    const fetchPokemon = async () => {
      const url = `${process.env.REACT_APP_API_HOST}/user/pokemon/protected`;
      fetch(url, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          setPokemon(data);
        })
        .catch((error) => console.error(error));
    };
    setTeamname(location.state.teamname)
    fetchPokemon()
  }, [location]);

  function handleChangePokemon1(event) {
    const { value } = event.target;
    setPokemon1(value);
  }
  function handleChangeTeamname(event) {
    const { value } = event.target;
    setTeamname(value);
  }


  return (
    <div className="row-form">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Edit {location.state.teamname}</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label className="form-label">Team Name</label>
              <input
                value={teamname}
                onChange={handleChangeTeamname}
                name="teamname"
                type="teamname"
                className="form-control"
              />
            </div>
            <select value={pokemon1} onChange={handleChangePokemon1} required name="pokemon1" id="pokemon1" className="form-select my-3 px-2">
              <option value="">Slot 1</option>
              {pokemon.map(pokemon => {
                return (
                  <option key={pokemon.id} value={pokemon.pokemon_id}>
                    {pokemon.pokemon}
                  </option>
                );
              })}
            </select>
            <div className="text-end">
              <input className="btn btn-dark" type="submit" value="Submit" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditTeam;
