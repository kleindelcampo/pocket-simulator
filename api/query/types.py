from pydantic import BaseModel
from query.pool import pool
from typing import List, Optional


class Error(BaseModel):
    message: str


class TypeOut(BaseModel):
    id: int
    image: str
    typename: str
    strengths: str
    weaknesses: str


class TypeRepository:
    def get_type(self, type_id: int) -> Optional[TypeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT  id, image, typename, strengths, weaknesses
                        FROM types
                        WHERE id = %s
                        """,
                        [type_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_type_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that type"}

    def get_types(self) -> List[TypeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT  id, image, typename, strengths, weaknesses
                        FROM types
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_type_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all types"}

    def record_to_type_out(self, record):
        return TypeOut(
            id=record[0],
            image=record[1],
            typename=record[2],
            strengths=record[3],
            weaknesses=record[4],
        )
