steps = [
    [

        """
        CREATE TABLE duels (
            id serial not null primary key,
            userid int not null,
            teamid int not null,
            enemyid int,
            enemyteamid int,
            winner varchar(25),
            date timestamp default current_timestamp
        );
        """,



        """
        DROP TABLE duels;
        """


    ],

]
