from pydantic import BaseModel
from query.pool import pool
from typing import List, Optional, Union
from datetime import datetime, date
import random


class DuelIn(BaseModel):
    userid: int
    teamid: int


class DuelsOut(BaseModel):
    duelid: int
    userid: int
    username: str
    teamname: str
    enemyimg: str
    enemyname: str
    winner: str
    date: date


class DuelCreateOut(BaseModel):
    id: int
    userid: int
    teamid: int
    enemyid: int
    enemyteamid: int
    winner: str
    date: date


class DuelPokemonInfo(BaseModel):
    name: str
    type: str
    strengths: str
    weaknesses: str
    picture: str


class DuelOut(BaseModel):
    id: int
    username: str
    team: str
    user_pokemon: DuelPokemonInfo
    enemyname: str
    enemy_pokemon: DuelPokemonInfo
    winner: str
    date: date


class Error(BaseModel):
    message: str


class DuelRepository:
    def get_duel(self, duel_id: int) -> Optional[DuelOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT duels.id, users.username AS Username,
                        teams.teamname AS Team, user_pokemon.name AS Pokemon,
                        user_pokemon.type AS Type,
                        user_pokemon_types.strengths AS Strengths,
                        user_pokemon_types.weaknesses AS Weaknesses,
                        user_pokemon.picture AS UserPokemonPicture,
                        enemies.enemyname, enemy_pokemon.name AS Pokemon,
                        enemy_pokemon.type AS Type,
                        enemy_pokemon_types.strengths AS Strengths,
                        enemy_pokemon_types.weaknesses AS Weaknesses,
                        enemy_pokemon.picture AS EnemyPokemonPicture,
                        winner, date
                        FROM duels
                        JOIN users ON duels.userid = users.id
                        JOIN teams ON duels.teamid = teams.id
                        JOIN enemies ON duels.enemyid = enemies.id
                        JOIN enemyteams ON
                        duels.enemyteamid = enemyteams.id
                        JOIN pokemon AS user_pokemon ON
                        teams.pokemon1 = user_pokemon.id
                        JOIN types AS user_pokemon_types ON
                        user_pokemon.type = user_pokemon_types.typename
                        JOIN pokemon AS enemy_pokemon ON
                        enemyteams.pokemon1 = enemy_pokemon.id
                        JOIN types AS enemy_pokemon_types ON
                        enemy_pokemon.type = enemy_pokemon_types.typename
                        WHERE
                        duels.id = %s
                        """,
                        [duel_id]
                    )
                    record = result.fetchone()
                    print(record)
                    if record is None:
                        return None
                    return self.record_to_duel_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that duel"}

    def get_duels(self, username: str) -> List[DuelsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT duels.id, duels.userid,
                        users.username, teams.teamname,
                        enemies.img, enemies.enemyname, winner, date
                        FROM users
                        JOIN duels on users.id = duels.userid
                        JOIN teams on duels.teamid = teams.id
                        JOIN enemies on duels.enemyid = enemies.id
                        WHERE users.username = %s
                        ORDER by date
                        """,
                        [username]
                    )
                    return [
                        self.record_to_duels_out(record) for record in result
                        ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all users"}

    def create_duel(self, duel: DuelIn) -> Union[DuelOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        "SELECT id FROM enemies ORDER BY RANDOM() LIMIT 1"
                        )
                    enemyid = db.fetchone()[0]

                    db.execute(
                        "SELECT id FROM enemyteams "
                        "WHERE enemyteamownerid = %s ORDER BY id LIMIT 1",
                        [enemyid]
                        )
                    enemyteamid = db.fetchone()[0]

                    winner = random.choice(["User", "Enemy"])
                    result = db.execute(
                        """
                        INSERT INTO duels
                            (userid,
                            teamid,
                            enemyid,
                            enemyteamid,
                            winner,
                            date)
                        VALUES
                            (%s, %s, %s, %s, %s, current_timestamp)
                         RETURNING id;
                        """,
                        [
                            duel.userid,
                            duel.teamid,
                            enemyid,
                            enemyteamid,
                            winner,
                        ]
                    )
                    id = result.fetchone()[0]
                    return self.duelcreate_in_to_out(id,
                                                     duel,
                                                     enemyid,
                                                     enemyteamid,
                                                     winner,
                                                     datetime.now())
        except Exception as e:
            print(e)
            return {"message": "Create did not work"}

    def duelcreate_in_to_out(self,
                             id: int,
                             duel: DuelIn,
                             enemyid: int,
                             enemyteamid: int,
                             winner: str,
                             date: datetime):
        return DuelCreateOut(
            id=id,
            userid=duel.userid,
            teamid=duel.teamid,
            enemyid=enemyid,
            enemyteamid=enemyteamid,
            winner=winner,
            date=date.date(),
        )

    def duel_in_to_out(self,
                       id: int,
                       duel: DuelIn,
                       enemyid: int,
                       enemyteamid: int,
                       winner: str):
        return DuelOut(
            id=id,
            userid=duel.userid,
            teamid=duel.teamid,
            enemyid=enemyid,
            enemyteamid=enemyteamid,
            winner=winner
        )

    def record_to_duels_out(self, record):
        return DuelsOut(
            duelid=record[0],
            userid=record[1],
            username=record[2],
            teamname=record[3],
            enemyimg=record[4],
            enemyname=record[5],
            winner=record[6],
            date=record[7]
        )

    def record_to_duel_out(self, record):
        user_pokemon_info = DuelPokemonInfo(
            name=record[3],
            type=record[4],
            strengths=record[5],
            weaknesses=record[6],
            picture=record[7]
        )

        enemy_pokemon_info = DuelPokemonInfo(
            name=record[9],
            type=record[10],
            strengths=record[11],
            weaknesses=record[12],
            picture=record[13]
        )

        return DuelOut(
            id=record[0],
            username=record[1],
            team=record[2],
            user_pokemon=user_pokemon_info,
            enemyname=record[8],
            enemy_pokemon=enemy_pokemon_info,
            winner=record[14],
            date=record[15]
        )
