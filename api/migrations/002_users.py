steps = [
    [
        
        """
        CREATE TABLE users (
            id serial not null primary key,
            picture varchar(500),
            username varchar(25) not null unique,
            email varchar(100) not null unique,
            firstname varchar(100) not null,
            lastname varchar(100) not null,
            hashedpassword varchar(100) not null
        );
        """,



        """
        DROP TABLE users;
        """


    ],
]
