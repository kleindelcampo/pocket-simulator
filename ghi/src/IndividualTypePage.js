import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function IndividualType() {
  const [type, setType] = useState("");
  const { id } = useParams()

  useEffect(() => {
      const getData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/types/${id}`);
    if (response.ok) {
      const data = await response.json();
      setType(data);
    }
  }
    getData();
  }, [id]);


  return (
    <div className="col-poke">
      <div className="offset-4 col-4">
        <div className="shadow bg-white p-4 mt-4">
          <h1>{type.typename}</h1>
          <img src={type.image} alt={type.typename} />
          <h5>ID</h5>
          <p>{type.id}</p>
          <h5>Type</h5>
          <p>{type.typename}</p>
          <h5>Strengths</h5>
          <p>{type.strengths}</p>
          <h5>Weaknesses</h5>
          <p>{type.weaknesses}</p>

        </div>
      </div>
    </div>
  );

}

export default IndividualType;
