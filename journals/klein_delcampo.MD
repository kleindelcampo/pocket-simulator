8th Jan - 12th Jan

- help setup repo for group
- loaded and setup docker for coding next week
- added onto wire frame using figma
- help create API endpoints for out project
- help discuss and added onto features to meet MVP

16th Jan - 19th Jan
- Created user and account models
- Worked on endpoints for queries and routers
- Setup database for our project
- Created table and verified using bee keeper
- Started working on authenticator


22nd Jan - 26th Jan
- Protected endpoints (get user and get duels)
- Joined users tables with teams and owner pokemon tables
- Completely finished endpoints
- Created Authenticator and authenticator logic
- Created endpoint for joining  the tables

23Jan - 2Feb
- Finished protecting multiple endpoints
- Created unit test for list pokemon type page
- Create front end page for list pokemon type
- Worked on protecting endpoints on the front end
- Debugged issue we were having on authorization
- Joined tables to create endpoints needed for MVP
