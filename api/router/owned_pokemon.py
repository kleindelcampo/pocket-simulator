from fastapi import APIRouter, Depends
from typing import Union
from query.owned_pokemon import (Error,
                                 OwnedPokemonIn,
                                 OwnedPokemonOut,
                                 OwnedPokemonRepository)

router = APIRouter()


@router.post("/ownedpokemon",
             response_model=Union[OwnedPokemonOut,
                                  Error])
def create_owned_pokemon(
    owned_pokemon: OwnedPokemonIn,
    repo: OwnedPokemonRepository = Depends(),
):
    return repo.create_owned_pokemon(owned_pokemon)
