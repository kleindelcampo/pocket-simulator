from fastapi import APIRouter, Depends
from typing import List, Optional, Union
from query.pokemon import (Error,
                           PokemonOut,
                           PokemonSingleOut,
                           PokemonRepository)

router = APIRouter()


@router.get("/pokemon", response_model=Union[List[PokemonOut], Error])
def get_list_pokemon(
    repo: PokemonRepository = Depends()
):
    return repo.get_list_pokemon()


@router.get("/pokemon/{pokemon_id}", response_model=Optional[PokemonSingleOut])
def get_pokemon(
        pokemon_id: int,
        repo: PokemonRepository = Depends(),
) -> Optional[PokemonSingleOut]:
    pokemon = repo.get_pokemon(pokemon_id)
    return pokemon


@router.get("/pokemon/type/{type}",
            response_model=Union[List[PokemonOut], Error])
def get_pokemon_by_type(
        type: str,
        repo: PokemonRepository = Depends()
):
    return repo.get_pokemon_by_type(type)


@router.get("/user/teams/{team_id}",
            response_model=Union[List[PokemonOut], Error])
def get_pokemon_by_team(
    team_id: int,
    repo: PokemonRepository = Depends(),
):
    pokemon = repo.get_pokemon_by_team(team_id)
    return pokemon
