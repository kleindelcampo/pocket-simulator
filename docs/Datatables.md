USERS TABLE
------------------------------------------------------------------------------------------

    """
        CREATE TABLE users (
            id serial not null primary key,
            picture varchar(500),
            username varchar(25) not null unique,
            email varchar(100) not null unique,
            firstname varchar(100) not null,
            lastname varchar(100) not null,
            hashedpassword varchar(100) not null
        );
    """


POKEMON TABLE
------------------------------------------------------------------------------------------

    """
		CREATE TABLE pokemon (
            id SERIAL PRIMARY KEY NOT NULL,
            picture VARCHAR(1000),
            name VARCHAR(255) NOT NULL UNIQUE,
            type VARCHAR(100) NOT NULL,
            description VARCHAR(1000)
        );
    """


TEAMS TABLE
------------------------------------------------------------------------------------------

    """
		CREATE TABLE teams (
            id SERIAL PRIMARY KEY NOT NULL,
            owner_id int,
            teamname VARCHAR(1000),
            pokemon1 int
        );
    """

TYPE TABLE
------------------------------------------------------------------------------------------

    """
		CREATE TABLE types (
            id SERIAL PRIMARY KEY NOT NULL,
            image VARCHAR(1000),
            typename VARCHAR(1000),
            strengths VARCHAR(1000),
            weaknesses VARCHAR(1000)
        );
    """


DUELS TABLE
------------------------------------------------------------------------------------------

    """
		CREATE TABLE duels (
            id serial not null primary key,
            userid int not null,
            teamid int not null,
            enemyid int,
            enemyteamid int,
            winner varchar(25),
            date timestamp default current_timestamp
        );
    """

ENEMIES TABLE
------------------------------------------------------------------------------------------

    
    """
			CREATE TABLE enemies (
	            id serial not null primary key,
	            img varchar(1000),
	            enemyname varchar(100) not null unique
	        );
	"""


ENEMY TEAMS TABLE

	
	"""
	        CREATE TABLE enemyteams (
	            id serial not null primary key,
	            enemyteamownerid int not null,
	            pokemon1 int
	        );
	"""
