from fastapi.testclient import TestClient
from main import app
from query.duels import DuelRepository

client = TestClient(app)


class EmptyDuelRepository:
    def get_duel(self, id: int):
        return {
            "id": 2,
            "username": "string3",
            "team": "test",
            "user_pokemon": {
              "name": "Piplup",
              "type": "Water",
              "strengths": "Fire",
              "weaknesses": "Electric",
              "picture": "https://assets.pokemon.com/assets6.png"
            },
            "enemyname": "Bruce Chan",
            "enemy_pokemon": {
              "name": "Hitmontop",
              "type": "Fighting",
              "strengths": "Normal",
              "weaknesses": "Flying",
              "picture": "https://assets.pokemon.com/assets9.png"
            },
            "winner": "User",
            "date": "2024-02-04"
            }


def test_get_single_duel():
    app.dependency_overrides[DuelRepository] = EmptyDuelRepository

    response = client.get("/duels/99")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
                        "id": 2,
                        "username": "string3",
                        "team": "test",
                        "user_pokemon": {
                          "name": "Piplup",
                          "type": "Water",
                          "strengths": "Fire",
                          "weaknesses": "Electric",
                          "picture": "https://assets.pokemon.com/assets6.png"
                        },
                        "enemyname": "Bruce Chan",
                        "enemy_pokemon": {
                          "name": "Hitmontop",
                          "type": "Fighting",
                          "strengths": "Normal",
                          "weaknesses": "Flying",
                          "picture": "https://assets.pokemon.com/assets9.png"
                        },
                        "winner": "User",
                        "date": "2024-02-04"
                      }
